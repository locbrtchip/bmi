$(document).on('click', '', function(e) {
	if(!$('[data-toggle="tooltip"]').length) {
		setTimeout(() => {
			$('[data-toggle="tooltip"]').tooltip()
		}, 2000);
	}
})
$(function () {
	setTimeout(() => {
		$('[data-toggle="tooltip"]').tooltip()
	}, 2000)
	$(document).on('click','[data-toggle="collapse"]', function(e) {
		let target = $(e.target).data('target');
		$(target).hasClass('show') ? $(target).collapse('hide') : null
	});
	$(document).on('focusin', function(e) {
		if ($(e.target).closest(".mce-window").length) {
			e.stopImmediatePropagation();
		}
	});
})
document.addEventListener('scroll', function (event) {
	$.each($("a:has(img)"), (key, value) => {
		if($(value).attr('href') == $(value).find('img').attr('src')) {
			$(value).attr('data-fancybox', 'gallery');
		}
	})
	$("[data-fancybox='gallery']").fancybox();
}, true);
window.addEventListener('beforeunload', function (e) {
	let confirmationMessage = "\o/";
	e.returnValue = confirmationMessage;
	return confirmationMessage;
});
window.addEventListener('popstate', function (e) {
	if ($('.modal').hasClass('show')) {
		$('.modal').modal('hide');
		history.go(1)
	}
});